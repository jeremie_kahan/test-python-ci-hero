import unittest
from main.model.Hero import Hero


class HeroTest(unittest.TestCase):
    heroTmp = ""

    @classmethod
    def setUpClass(cls):
        """Preparing the current TestClass, launch one time before the TestClass start"""
        print("--Preparing the current TestClass")
        cls.heroTmp = Hero("HERO_TMP")

    @classmethod
    def tearDownClass(cls):
        """Clearing the current TestClass, launch one time after the TestClass end"""
        print("--Clearing the current TestClass \n")
        cls.heroTmp = None

    def setUp(self):
        """ Preparing current test method, launch before each test function"""
        print("------Preparing the current test method \n")
        if self.heroTmp._name != "HERO_TMP":
            self.heroTmp = Hero("HERO_TMP")

    def tearDown(self):
        """ Cleaning current test method results, launch after each test function"""
        print("------Cleaning the current test method \n")

    def test_create_hero(self):
        print("------ ------ test_create_hero TEST \n")
        self.heroTmp = Hero("ME")
        self.assertIsNotNone(self.heroTmp)

    def test_all_params(self):
        print("------ ------ test_all_params TEST \n")
        self.heroTmp.set_all_params("ME1","super pink","water","fire",100,50,20,3,10,1)
        self.assertEqual(self.heroTmp._name,"ME1"," name not set correctly")
        self.assertEqual(self.heroTmp._super_power, "super pink", " superpower not set correctly")
        self.assertEqual(self.heroTmp._elemental_affinity, "water", " elementalAffinity not set correctly")
        self.assertEqual(self.heroTmp._elemental_vulnerability, "fire", " elementalVulnerability not set correctly")
        self.assertEqual(self.heroTmp._hp, 100, " hp not set correctly")
        self.assertEqual(self.heroTmp._energy, 50, " energy not set correctly")
        self.assertEqual(self.heroTmp._attack, 20, " attack not set correctly")
        self.assertEqual(self.heroTmp._attack_variance, 3, " attackVariance not set correctly")
        self.assertEqual(self.heroTmp._defense, 10, " defense not set correctly")
        self.assertEqual(self.heroTmp._defense_std, 1, " defenseStd not set correctly")


if __name__ == '__main__':
    unittest.main()
