import unittest
from main.model.Hero import Hero


class SimpleTestHero(unittest.TestCase):

    def test_hero_creation(self):
        h=Hero("test")
        self.assertFalse(h, None)

    def test_hero_name_init(self):
        h = Hero("test")
        self.assertEqual(h.get_name(), "test")


if __name__ == '__main__':
    unittest.main()

